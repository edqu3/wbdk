package wbdk;

import java.util.HashMap;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author edqu3
 */
public class ScreenManager {
    
    private final HashMap<String, Screen> screens;
    
    // singleton
    public static ScreenManager INSTANCE = new ScreenManager();
    
    private ScreenManager() {
        screens = new HashMap<>();
    }
    
    /**
     * >
     * @param key
     * @param screen 
     */
    public void storeScreen(String key, Screen screen){
        screens.put(key, screen);
    }
    
    /**
     * >
     * @param key
     * @return 
     */
    public Screen getScreen(String key){
        return screens.get(key);
    }
    
    /**
     * Displays a temporary dialog.
     * Assumes that only 1 dialog can be displayed at a time.
     * @param dialog
     * @param param
     * @param owner
     * @return 
     */
    public Controller showDialog(String dialog, DialogParameters param, Stage owner){
        
        Screen screen = getDialog(dialog, param);
        Parent root = screen.getRoot();
        
        // scene is only created once.
        Scene scene = ( root.getScene() == null ) ? new Scene(root) : root.getScene();
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(owner);
        stage.setScene(scene);
        stage.showAndWait();
        
        return screen.getController();
    }

    public void storeDialog(String key, Screen dialog) {
        storeScreen(key, dialog);
    }

    private Screen getDialog(String key, DialogParameters param ) {
        // Build dialog based on the type of dialog parameters passed in.
        Screen screen = getScreen(key);
        
        //Parent     root       = screen.getRoot();
        Controller controller = screen.getController();
        controller.setParameters(param);
        
        return screen;
    }
    
}
