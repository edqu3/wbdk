package wbdk;

import wbdk.data.Player;

/**
 *
 * @author edqu3
 */
public class ConfirmDialogParameters extends DialogParameters {
    
    private String message;
    private String yesButtonText;
    private String noButtonText;
    private final Player player;
    
    public ConfirmDialogParameters(){
        this.player = null;
    }
    
    public ConfirmDialogParameters(Player player){
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
    
    public ConfirmDialogParameters setMessage(String message) {
        this.message = message;
        return this;
    }

    public ConfirmDialogParameters setNoButtonText(String noButtonText) {
        this.noButtonText = noButtonText;
        return this;
    }

    public ConfirmDialogParameters setYesButtonText(String yesButtonText) {
        this.yesButtonText = yesButtonText;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public String getNoButtonText() {
        return noButtonText;
    }

    public String getYesButtonText() {
        return yesButtonText;
    }
    
}
