package wbdk;

import wbdk.data.Player;

/**
 *
 * @author edqu3
 */
public class EditPlayerDialogParameters extends DialogParameters{

    private final Player player;
    
    public EditPlayerDialogParameters() {
        this.player = null;
    }

    public EditPlayerDialogParameters(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
    
}
