package wbdk;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class WBDK extends Application {
    private final String TITLE = "Wolfie Ball Draft Kit";
    
    @Override
    public void start(Stage stage) throws Exception {
        
        initScreens();
        initDialogs();
        
        Scene scene = new Scene(
            ScreenManager.INSTANCE.getScreen(
                Screen.INTERFACE_SCREEN).getRoot());
        
        stage.setWidth(800);
        stage.setHeight(600);
        stage.setTitle(TITLE);
        stage.setScene(scene);
        stage.show();
    }
    
    // Load all parent nodes and their controllers
    private void initScreens(){
        ScreenManager.INSTANCE.storeScreen(Screen.INTERFACE_SCREEN, 
                loadFXML("views/Interface.fxml"));
        ScreenManager.INSTANCE.storeScreen(Screen.PLAYERS_SCREEN,
                loadFXML("views/PlayersScreen.fxml"));
        ScreenManager.INSTANCE.storeScreen(Screen.TEAMS_SCREEN,
                loadFXML("views/TeamsScreen.fxml"));
        ScreenManager.INSTANCE.storeScreen(Screen.STANDINGS_SCREEN,
                loadFXML("views/StandingsScreen.fxml"));
        ScreenManager.INSTANCE.storeScreen(Screen.DRAFT_SCREEN,
                loadFXML("views/DraftScreen.fxml"));
        ScreenManager.INSTANCE.storeScreen(Screen.MLBTEAMS_SCREEN, 
                loadFXML("views/MLBTeamsScreen.fxml"));
    }

    private void initDialogs(){
        ScreenManager.INSTANCE.storeDialog(Screen.MESSAGE_DIALOG,
                loadFXML("views/MessageDialog.fxml"));
        ScreenManager.INSTANCE.storeDialog(Screen.CONFIRM_DIALOG,
                loadFXML("views/ConfirmDialog.fxml"));
        ScreenManager.INSTANCE.storeDialog(Screen.EDIT_PLAYER_DIALOG,
                loadFXML("views/EditPlayerDialog.fxml"));
        ScreenManager.INSTANCE.storeDialog(Screen.ADD_PLAYER_DIALOG,
                loadFXML("views/AddPlayerDialog.fxml"));
        ScreenManager.INSTANCE.storeDialog(Screen.FANTASY_TEAM_DIALOG,
                loadFXML("views/FantasyTeamDialog.fxml"));
        
        
    }
    
    /**
     * returns an FXML object and its controller.
     * @param path
     * @return Screen - wrapper for getting Node and controller of a screen.
     */
    private Screen loadFXML(String path){
        FXMLLoader loader         = new FXMLLoader(getClass().getResource(path));
        try {
            Parent load           = loader.load();
            Controller controller = loader.getController();
            return new Screen(load, controller);
        } 
        catch (IOException ex) {
            Logger.getLogger(WBDK.class.getName()).log(Level.SEVERE, null, ex); 
        }
        return null;
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
