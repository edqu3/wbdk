package wbdk;

import javafx.scene.Parent;
import javafx.stage.Stage;

/**
 * Stores screen node and its controller.
 * @author edqu3
 */
public class Screen{
    
    //<editor-fold defaultstate="collapsed" desc="Screens">
    public final static String INTERFACE_SCREEN    = "Interface.fxml";
    public final static String TEAMS_SCREEN        = "TeamsScreen.fxml";
    public final static String PLAYERS_SCREEN      = "PlayersScreen.fxml";
    public final static String STANDINGS_SCREEN    = "StandingsScreen.fxml";
    public final static String DRAFT_SCREEN        = "DraftScreen.fxml";
    public final static String MLBTEAMS_SCREEN     = "MLBTeamsScreen.fxml";
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Dialogs">
    public final static String MESSAGE_DIALOG      = "MessageDialog.fxml";
    public final static String CONFIRM_DIALOG      = "ConfirmDialog.fxml";
    public final static String EDIT_PLAYER_DIALOG  = "EditPlayerDialog.fxml";
    public final static String ADD_PLAYER_DIALOG   = "AddPlayerDialog.fxml";
    public final static String FANTASY_TEAM_DIALOG = "FantasyTeamDialog.fxml";
    
    //</editor-fold>    
    
    private final Parent root;
    private final Controller controller;
    
    public Screen(Parent root, Controller controller){
        this.root = root;
        this.controller = controller;
    }
    
    public Parent getRoot() {
        return root;
    }
    
    public Controller getController() {
        return controller;
    }
    
    public Stage getStage(){
        return (Stage) root.getScene().getWindow();
    }
    
}