package wbdk;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wbdk.controllers.FantasyTeamDialogController;
import wbdk.controllers.ConfirmDialogController;
import wbdk.data.Player;
import wbdk.data.FantasyTeam;
import wbdk.data.PlayerPosition;
import wbdk.file.JsonFileManager;

public class DataManager {
    
    // Singleton
    public static DataManager INSTANCE = new DataManager();
    
    private final ObservableList<FantasyTeam> backEndFantasyTeams;
    
    // DATA
    private TableView<Player> startingLineUpTable;
    
    // originalData contains the list of players loaded when the program is started up.
    private final ObservableList<Player> backEndPlayerPool;
    
    // this is the variable that keeps track of what team we are currently working on. (important for draft screen)
    private FantasyTeam workingOnFantasyTeam;
    
    private DataManager() {
        backEndPlayerPool   = FXCollections.observableArrayList();
        backEndFantasyTeams = FXCollections.observableArrayList();
    }
    
    public ObservableList<Player> getBackEndPlayerList() {
        return backEndPlayerPool;
    }
    
    public void addPlayerToBackEndList(){
        // create Add Player Dialog.
        Stage owner = ScreenManager.INSTANCE.getScreen(Screen.PLAYERS_SCREEN).getStage();
        ScreenManager.INSTANCE.showDialog(Screen.ADD_PLAYER_DIALOG, new AddPlayerDialogParameters(), owner);
        // control is transfered to the  dialog.
    }
        
    /**
     * should only be called by addPlayerDialog
     * @param p 
     */
    public void addPlayerToPool(Player p){
        backEndPlayerPool.add(p);
    }
    
    /**
     * Load players into GUI.
     * TODO: input path manually or read path from a property file.
     */
    public void loadPlayers(){
        
        String pitchersPath = "src/data/Pitchers.json";
        String arrayKey1 = "Pitchers";
        // fetch data
        
        JsonFileManager jfm  = new JsonFileManager(pitchersPath);
        backEndPlayerPool.addAll(jfm.getPitchers(arrayKey1));    // arrayKey is hitters or pitchers
        
        String hittersPath = "src/data/Hitters.json";
        String arrayKey2 = "Hitters";
        
        jfm  = new JsonFileManager(hittersPath);
        backEndPlayerPool.addAll(jfm.getHitters(arrayKey2));    // arrayKey is hitters or pitchers
        
    }
    
    public void removePlayerFromBackEndList(Player player){
        Stage owner = ScreenManager.INSTANCE.getScreen(Screen.PLAYERS_SCREEN).getStage();
        ConfirmDialogParameters confirmDialogParameters = 
                new ConfirmDialogParameters(player).setNoButtonText("No").setYesButtonText("Yes");
                confirmDialogParameters.setMessage(
                    "Are you sure you want to remove " +
                    player.getFirstNameColumn().get()  + " " +
                    player.getLastNameColumn().get()   + " from the list?");
        ScreenManager.INSTANCE.showDialog(Screen.CONFIRM_DIALOG, confirmDialogParameters, owner);
        // control is transfered to the  dialog.
    }
    
    public void removePlayer(Player player) {
        backEndPlayerPool.remove(player);
    }
    
    public void editPlayerFromBackEndList(Player player){
        Stage owner = ScreenManager.INSTANCE.getScreen(Screen.PLAYERS_SCREEN).getStage();
        
        if (this.backEndFantasyTeams.isEmpty()) {
            // if no teams have been created then display a message to the user.
            MessageDialogParamaters mDialogParam = new MessageDialogParamaters();
            mDialogParam.setMessage("You have to create and select a Fantasy Team before drafting.");
            mDialogParam.setButtonText("ok");
            ScreenManager.INSTANCE.showDialog(Screen.MESSAGE_DIALOG, mDialogParam, owner);
        }
        else{
            // else continue
            EditPlayerDialogParameters editPlayerDialogParameters =
                    new EditPlayerDialogParameters(player);
            ScreenManager.INSTANCE.showDialog(Screen.EDIT_PLAYER_DIALOG, editPlayerDialogParameters, owner);
            // control is transfered to the  dialog.
        }
    }

    public List<FantasyTeam> getTeams() {
        return this.backEndFantasyTeams;
    }

    public void addTeam(Stage owner) {
        
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/wbdk/views/FantasyTeamDialog.fxml"));
            Parent node = loader.load();
            // load player data
            FantasyTeamDialogController controller = loader.getController();
            // show dialog
            Scene scene = new Scene(node);
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(owner);
            stage.setScene(scene);
            stage.showAndWait();
            
            if (controller.getTeam() != null) {
                backEndFantasyTeams.add(controller.getTeam());
            }
            
        } catch (IOException ex) {
            Logger.getLogger(DataManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private FantasyTeam getTeamFromName(String teamName){
        for (FantasyTeam fantasyTeam : backEndFantasyTeams) {
            if(fantasyTeam.getTeamName().equals(teamName)){
                return fantasyTeam;   
            }
        }
        return null;
    }
    
    /**
     * works
     * @param team
     * @param owner 
     */
    public void removeTeam(String team, Stage owner) {
        
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/wbdk/views/ConfirmDialog.fxml"));
            Parent node = loader.load();
            // get controller
            ConfirmDialogController controller = loader.getController();
            // show dialog
            Scene scene = new Scene(node);
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(owner);
            stage.setScene(scene);
            stage.showAndWait();
            // ask controller for the users input.
            
            FantasyTeam t = getTeamFromName(team);
//            if (controller.getResponse() && t != null) {
//                fantasyTeams.remove(t);
//            }
            
        } catch (IOException ex) {
            Logger.getLogger(DataManager.class.getName()).log(Level.SEVERE, null, ex);
        }        
                
    }

    /**
     * works
     * @param team
     * @param owner 
     */
    public void editTeam(String team, Stage owner) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/wbdk/views/FantasyTeamDialog.fxml"));
            Parent node = loader.load();
            // get controller
            FantasyTeamDialogController controller = loader.getController();
            
            controller.setTeam( getTeamFromName(team) );
            
            // show dialog
            Scene scene = new Scene(node);
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(owner);
            stage.setScene(scene);
            stage.showAndWait();
            
        } catch (IOException ex) {
            Logger.getLogger(DataManager.class.getName()).log(Level.SEVERE, null, ex);
        }                
    }
    
    /**
     * called when a player in the Players screen is set to a fantasy team.
     * Adds or updates the player in the fantasy teams table.
     * @param player 
     */
    public void updatePlayerInTeam(Player player) {
        
        for (FantasyTeam fantasyTeam : backEndFantasyTeams) {
            if (player.getCurrentFantasyTeam().equals(fantasyTeam.getTeamName())) {
                // add or update in table.
                fantasyTeam.addPlayer(player, PlayerPosition.valueOf(player.getPlayingPosition().get()));
                
                // TODO: use ScreenManager instead
//                TeamsScreenController controller = (TeamsScreenController) controllers.get(DataManager.TEAMS_SCREEN);
//                controller.updateTable(FXCollections.observableArrayList(fantasyTeam.getPlayers()));
                break;
            }
        }
        
     }

    public void setFantasyTeamsTable(TableView<Player> startingLineUpTableView) {
        this.startingLineUpTable = startingLineUpTableView;
    }
}