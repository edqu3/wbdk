package wbdk.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import wbdk.DataManager;
import wbdk.Controller;
import wbdk.DialogParameters;
import wbdk.data.Player;

public class PlayersController implements Initializable, Controller{
    
    //<editor-fold defaultstate="collapsed" desc="controls">
    @FXML
    private Button addPlayerButton;
    @FXML
    private Button removePlayerButton;
    @FXML
    private TextField searchTextField;
    //<editor-fold defaultstate="collapsed" desc="radio buttons">
    @FXML
    private ToggleGroup filterGroup;
    @FXML
    private RadioButton allRadioButton;
    @FXML
    private RadioButton catcherRadioButton;
    @FXML
    private RadioButton firstBasemanRadioButton;
    @FXML
    private RadioButton cornerInfielderRadioButton;
    @FXML
    private RadioButton thirdBaseManRadioButton;
    @FXML
    private RadioButton secondBaseManRadioButton;
    @FXML
    private RadioButton middleInfielderRadioButton;
    @FXML
    private RadioButton shortstopRadioButton;
    @FXML
    private RadioButton outfielderRadioButton;
    @FXML
    private RadioButton utilityRadioButton;
    @FXML
    private RadioButton pitcherRadioButton;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="table controls">
    @FXML
    private TableView<Player> playersTableView;
    @FXML
    private TableColumn<Player, String> firstNameColumn;
    @FXML
    private TableColumn<Player, String> lastNameColumn;
    @FXML
    private TableColumn<Player, String> proTeamColumn;
    @FXML
    private TableColumn<Player, String> positionsColumn;
    @FXML
    private TableColumn<Player, Number> yearOfBirthColumn;
    @FXML
    private TableColumn<Player, Number> r_wColumn;
    @FXML
    private TableColumn<Player, Number> hr_svColumn;
    @FXML
    private TableColumn<Player, Number> rbi_kColumn;
    @FXML
    private TableColumn<Player, Number> sb_eraColumn;
    @FXML
    private TableColumn<Player, Number> ba_whipColumn;
    @FXML
    private TableColumn<Player, Number> estimatedValueColumn;
    @FXML
    private TableColumn<Player, String> notesColumn;
    //</editor-fold>

    //</editor-fold>
        
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        initTable();
        
        initToggleGroup();
        
        initSearch();
        
        // tripple click player's row to edit player
        playersTableView.setRowFactory((TableView<Player> param) -> {
            TableRow<Player> row = new TableRow<>();
            row.setOnMouseClicked((MouseEvent event) -> {
                if (event.getClickCount() == 2 && !row.isEmpty()) {
                    editPlayer(row.getItem());
                }
            });
            return row;
        });
        
    }
    
    /**
     * Filter types of Players when a Toggle is changed
     * @param toggle 
     */
    private void filterPlayersByPosition(Toggle toggle, FilteredList<Player> filteredData){
        // set column titles
        setColumns(toggle);
        // filter data based on toggle selection
        filteredData.setPredicate(new Predicate<Player>() {

            @Override
            public boolean test(Player t) {
                // test for player with undefined position, but a player must always have a position before being entered into the table.
                if (t.getPositionsColumn().isNull().getValue()) {
                    return false;
                }
                
                String position0 = " ";
                String position1 = " ";
                
                if      (toggle == allRadioButton) {
                    position0 = "ALL";
                }
                else if (toggle == catcherRadioButton) {
                    position0 = "C";
                }
                else if (toggle == cornerInfielderRadioButton){
                    position0 = "1B";
                    position1 = "3B";
                }
                else if (toggle == firstBasemanRadioButton) {
                    position0 = "1B";
                }
                else if (toggle == middleInfielderRadioButton){
                    position0 = "2B";
                    position1 = "SS";
                }
                else if (toggle == outfielderRadioButton){
                    position0 = "OF";
                }
                else if (toggle == pitcherRadioButton){
                    position0 = "P";
                }
                else if (toggle == secondBaseManRadioButton){
                    position0 = "2B";
                }
                else if (toggle == shortstopRadioButton){
                    position0 = "SS";
                }
                else if (toggle ==  thirdBaseManRadioButton){
                    position0 = "3B";
                }
                else if (toggle == utilityRadioButton){
                    position0 = "U";
                }
                
                if (position0.equals("ALL")) {
                    return true;
                }
                else if (position0.equals("U")) {
                    // All Players that are not Pitchers
                    return !t.getPositionsColumn().get().contains("P");
                }
                else if (position0.equals("P")) {
                    // All Players that are Pitchers.
                    return t.getPositionsColumn().get().contains("P");
                }
                else{
                    boolean eligible = (
                            t.getPositionsColumn().get().contains(position0) ||
                            t.getPositionsColumn().get().contains(position1));
                    return eligible;
                }
            }
            
        });
    }
    
    private void initToggleGroup(){
        
        FilteredList<Player> filteredData = new FilteredList<>(playersTableView.getItems(), (Player p) -> true);
        
        // filter table when a new toggle is selected
        filterGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                filterPlayersByPosition(newValue, filteredData);
            }
        });
        
        
        // wrap list so it is sortable.
        SortedList<Player> sortedData = new SortedList(filteredData);
        // bind comparators
        sortedData.comparatorProperty().bind(playersTableView.comparatorProperty());
        
        playersTableView.setItems(sortedData);        
        
        // default selection on first load.
        filterGroup.selectToggle(allRadioButton);
    }
    
    private void initTable(){
        // Player Properties that are inherited by both Pitchers and Hitters.
        // These properties are not Pitcher/Hitter specific so they are only set once.
        firstNameColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                   param.getValue().getFirstNameColumn());
        lastNameColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                        param.getValue().getLastNameColumn());
        proTeamColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                        param.getValue().getTeamColumn());
        positionsColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                        param.getValue().getPositionsColumn());
        yearOfBirthColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) -> 
                        param.getValue().getYearOfBirthColumn());
        estimatedValueColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) -> 
                        param.getValue().getEstimatedValueColumn());
        notesColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                        param.getValue().getNotesColumn());
        // add editable textfield to note cells.
        notesColumn.setCellFactory (TextFieldTableCell.forTableColumn());
        notesColumn.setOnEditCommit((CellEditEvent<Player, String> event) -> {
            event.getTableView().getItems().get( event.getTablePosition().getRow()).setNotes(event.getNewValue());
            event.consume();
        });
        
        // TODO:
        // implement an interface that extracts the corresponding Pitcher or Hitter stat
        // from a Player object.
        
        // The differentation of Pitchers and Hitters is not made in this method, the filtering of Pitchers and Hitters
        // is made externally. This method can recieve any of the following:
        // an ALL Pitcher's list, an ALL Hitter's list, ALL Players,
        // or a List of Filtered Players based on certain criteria, such as when using the search method.
        r_wColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) -> 
                        param.getValue().getR_WColumn());
        hr_svColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) -> 
                        param.getValue().getHR_SVColumn());
        rbi_kColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) -> 
                        param.getValue().getRBI_KColumn());
        sb_eraColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) -> 
                        param.getValue().getSB_ERAColumn());
        ba_whipColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) -> 
                        param.getValue().getBA_WHIPColumn());
        
        // load all players, this is the first time the table is populated with all players
        playersTableView.setItems(DataManager.INSTANCE.getBackEndPlayerList());
        
    }
    
    private void initSearch(){
        
        // wrap in filtered list, add all unconditionaly
        FilteredList<Player> filteredData = new FilteredList<>(playersTableView.getItems(), (Player p) -> true);
        
        // listen for text inputs.
        searchTextField.textProperty().addListener(
            (ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                // match first name or last name player.
                filteredData.setPredicate((Player t) -> {
                    if (newValue == null || newValue.isEmpty()) {
                        return true;
                    }
                    
                    String lowerCaseFilter = newValue.toLowerCase();
                    if (t.getFirstNameColumn().get().toLowerCase().startsWith(lowerCaseFilter)) {
                        return true; 
                    } else if (t.getLastNameColumn().get().toLowerCase().startsWith(lowerCaseFilter)) {
                        return true; 
                    }
                    return false;
                });
        });
        
        // wrap list so it is sortable.
        SortedList<Player> sortedData = new SortedList(filteredData);
        // bind comparators
        sortedData.comparatorProperty().bind(playersTableView.comparatorProperty());
        
        playersTableView.setItems(sortedData);
    }
    
    @FXML
    public void addPlayer(ActionEvent e) {
        DataManager.INSTANCE.addPlayerToBackEndList();
    }
        
    @FXML
    public void removePlayer(ActionEvent e){
        Player player = playersTableView.getSelectionModel().getSelectedItem();
        DataManager.INSTANCE.removePlayerFromBackEndList(player);
    }
    
    public void editPlayer(Player player){
        DataManager.INSTANCE.editPlayerFromBackEndList(player);
        
        
//        try {
//            // table selected player 
//            dataManager.editPlayer(player, (Stage)ScreenManager.INSTANCE.getScreen(ScreenManager.PLAYERS_SCREEN).getRoot().getScene().getWindow() );
//            
////            dataManager.editPlayer(player, (Stage)dataManager.getScreen(DataManager.PLAYERS_SCREEN).getScene().getWindow() );
//            // match with back end player list
//            
//            // load data from back end list to dialog
//            
//            // observe changes
//            
//            // update if necessary
//            
//            loadPlayersIntoTable(dataManager.getAll(ColumnFilter.All));
//            
//        } catch (IOException ex) {
//            Logger.getLogger(PlayersController.class.getName()).log(Level.SEVERE, null, ex);
//        }

    }
    
    private void setColumns(Toggle toggle){
        // only called when toggle changes
        
        if (toggle == allRadioButton) {
            r_wColumn.setText("R/W");
            hr_svColumn.setText("HR/SV");
            rbi_kColumn.setText("RBI/K");
            sb_eraColumn.setText("SB/ERA");
            ba_whipColumn.setText("BA/WHIP");
            
        }
        else if (toggle == pitcherRadioButton) {
            r_wColumn.setText("W");
            hr_svColumn.setText("SV");
            rbi_kColumn.setText("K");
            sb_eraColumn.setText("ERA");
            ba_whipColumn.setText("WHIP");            
        }
        else{
            // Utilies
            r_wColumn.setText("R");
            hr_svColumn.setText("HR");
            rbi_kColumn.setText("RBI");
            sb_eraColumn.setText("SB");
            ba_whipColumn.setText("BA");
        }
        
    }
    
    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setParameters(DialogParameters a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
