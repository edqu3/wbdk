package wbdk.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import wbdk.Controller;
import wbdk.DialogParameters;
import wbdk.data.FantasyTeam;

/**
 *
 * @author edqu3
 */
public class StandingsController implements Initializable, Controller{
    //<editor-fold defaultstate="collapsed" desc="controls">
    @FXML private TableView<FantasyTeam>           standingsTableView;
    @FXML private TableColumn<FantasyTeam, String> teamNameColumn;
    @FXML private TableColumn<FantasyTeam, Number> playersNeededColumn;
    @FXML private TableColumn<FantasyTeam, Number> fundsLeftColumn;
    @FXML private TableColumn<FantasyTeam, Number> ppColumn;          // $PP = $Left / PlayersNeeded
    @FXML private TableColumn<FantasyTeam, Number> runsColumn;
    @FXML private TableColumn<FantasyTeam, Number> homerunsColumn;
    @FXML private TableColumn<FantasyTeam, Number> runsBattedInColumn;
    @FXML private TableColumn<FantasyTeam, Number> stolenBasesColumn;
    @FXML private TableColumn<FantasyTeam, Number> battingAverageColumn;
    @FXML private TableColumn<FantasyTeam, Number> winsColumn;
    @FXML private TableColumn<FantasyTeam, Number> strikeOutsColumn;
    @FXML private TableColumn<FantasyTeam, Number> savesColumn;
    @FXML private TableColumn<FantasyTeam, Number> eraColumn;
    @FXML private TableColumn<FantasyTeam, Number> whipColumn;
    @FXML private TableColumn<FantasyTeam, Number> totalPoints;
    //</editor-fold>
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        initTable();
//        List<Player> playersList = DataManager.INSTANCE.getPlayersList(); 
//        List<FantasyTeam> teams = new ArrayList<>();
//        
//        FantasyTeam myteam       = new FantasyTeam("Me", "Great Team");
//        for (int i = 0; i < 24; i++) {
//            myteam.addPlayer(playersList.get(i), PlayerPosition.CATCHER);
//        }
//        
//        FantasyTeam myteam0       = new FantasyTeam("Me", "Second best Team");
//        for (int i = 10; i < 30; i++) {
//            myteam0.addPlayer(playersList.get(i), PlayerPosition.PITCHER);
//        }
//        teams.add(myteam);
//        teams.add(myteam0);
//        standingsTableView.getItems().setAll(teams);
        
    }
    
    private void initTable(){
        // set columns
        teamNameColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, String> param) -> 
                        param.getValue().getTeamName());
        
        playersNeededColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) -> 
                        param.getValue().getPlayerNeeded());
        
        fundsLeftColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) -> 
                        param.getValue().getFunds());
        
        ppColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) -> 
                        param.getValue().getPricePerPlayer());
        
        runsColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) -> 
                        param.getValue().getRuns());
        
        homerunsColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getHomeRuns());
        
        runsBattedInColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getRunsBattedIn());
        
        stolenBasesColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getStolenBases());
        
        battingAverageColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getBattingAverage());
        
        winsColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getWins());
        
        strikeOutsColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getStrikeOuts());
        
        savesColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getSaves());
        
        eraColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getEra());
        
        whipColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getWhip());
        
        totalPoints.setCellValueFactory(
                (TableColumn.CellDataFeatures<FantasyTeam, Number> param) ->
                        param.getValue().getPoints());
        // get all fantasy teams created
        //standingsTableView.setItems(FXCollections.observableArrayList());
    }
    
    
    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setParameters(DialogParameters a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
