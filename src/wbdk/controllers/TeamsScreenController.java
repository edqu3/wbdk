package wbdk.controllers;

import java.net.URL;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.util.Callback;
import wbdk.ControllerType;
import wbdk.DataManager;
import wbdk.Controller;
import wbdk.DialogParameters;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;
import wbdk.data.Player;
import wbdk.data.FantasyTeam;

/**
 * FXML Controller class
 *
 * @author edqu3
 */
public class TeamsScreenController implements Initializable, ControllerType, Controller {
    
    //<editor-fold defaultstate="collapsed" desc="controls">
    @FXML
    private Button addTeamButton;
    @FXML
    private Button removeTeamButton;
    @FXML
    private Button editTeamButton;
    @FXML
    private ComboBox<String> selectTeamComboBox;
    @FXML
    private TableView<Player> startingLineUpTableView;
    @FXML
    private TableColumn<Player, String> positionColumn;
    @FXML
    private TableColumn<Player, String> firstNameColumn;
    @FXML
    private TableColumn<Player, String> lastNameColumn;
    @FXML
    private TableColumn<Player, String> teamColumn;
    @FXML
    private TableColumn<Player, String> positionsColumn;
    @FXML
    private TableColumn<Player, Number> r_wColumn;
    @FXML
    private TableColumn<Player, Number> hr_svColumn;
    @FXML
    private TableColumn<Player, Number> rbi_kColumn;
    @FXML
    private TableColumn<Player, Number> sb_eraColumn;
    @FXML
    private TableColumn<Player, Number> ba_whipColumn;
    @FXML
    private TableColumn<Player, Number> estimatedValueColumn;
    @FXML
    private TableColumn<Player, String> contractColumn;
    @FXML
    private TableColumn<Player, Number> salaryColumn;
//</editor-fold>
    
    private ObservableList<String> data;
    private ObservableList<Player> players;

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setParameters(DialogParameters a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        selectTeamComboBox.setItems(FXCollections.observableArrayList());
        DataManager.INSTANCE.setFantasyTeamsTable(this.startingLineUpTableView);
        
        positionColumn.setComparator(new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                
                return Collator.getInstance().compare(o1, o2);
            }
        });
    }

    @FXML
    private void newTeam(ActionEvent event) {
        DataManager.INSTANCE.addTeam((Stage)addTeamButton.getScene().getWindow());
        
        loadTeams();
    }

    @FXML
    private void removeTeam(ActionEvent event) {
        // TODO: call simpleYesNoDialog to confirm deletion
        String team =  selectTeamComboBox.getSelectionModel().getSelectedItem();
        DataManager.INSTANCE.removeTeam(team,(Stage)addTeamButton.getScene().getWindow());
        
        loadTeams();

    }

    @FXML
    private void editTeam(ActionEvent event) {
        String team = selectTeamComboBox.getSelectionModel().getSelectedItem();
        DataManager.INSTANCE.editTeam(team,(Stage)addTeamButton.getScene().getWindow());
        
        loadTeams();
    }
    
    /**
     * call each time an update in the combo box is needed.
     */
    private void loadTeams(){
        // get all the team names, and load them into the combo box.
                
        List<FantasyTeam> list = DataManager.INSTANCE.getTeams();
        List<String> teamName = new ArrayList<>();
        
        for (FantasyTeam l : list) { teamName.add(l.getTeamName().get()); }
        
        data = FXCollections.observableArrayList(teamName);
        selectTeamComboBox.setItems(data);
    }

    @Override
    public void updateTable(List<Player> list) {
        setColumns();
        players = FXCollections.observableArrayList(list);
        startingLineUpTableView.setItems(players);

    }
    
    /**
     * Stats are always combined a combination of Hitters and Pitchers stats.
     */
    private void setColumns(){

        positionColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, String> param) ->
                param.getValue().getPlayingPosition());
        
        firstNameColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, String> param) -> 
                param.getValue().getFirstNameColumn());
        
        lastNameColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, String> param) -> 
                param.getValue().getLastNameColumn());
        
        teamColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, String> param) -> 
                param.getValue().getTeamColumn());
        
        positionsColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, String> param) -> 
                param.getValue().getPositionsColumn());

        r_wColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, Number> param) -> 
                param.getValue().getR_WColumn());
        
        hr_svColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, Number> param) -> 
                param.getValue().getHR_SVColumn());
        
        rbi_kColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, Number> param) -> 
                param.getValue().getRBI_KColumn());
        
        sb_eraColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, Number> param) -> 
                param.getValue().getSB_ERAColumn());
        
        ba_whipColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, Number> param) -> 
                param.getValue().getBA_WHIPColumn());
        
        estimatedValueColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, Number> param) -> 
                param.getValue().getEstimatedValueColumn());
        
        contractColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, String> param) -> 
                param.getValue().getContract());
   
        salaryColumn.setCellValueFactory(
            (TableColumn.CellDataFeatures<Player, Number> param) ->
                param.getValue().getSalary());
        
    }
}
