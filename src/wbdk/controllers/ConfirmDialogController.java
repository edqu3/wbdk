package wbdk.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import wbdk.ConfirmDialogParameters;
import wbdk.Controller;
import wbdk.DataManager;
import wbdk.Screen;
import wbdk.ScreenManager;

/**
 * FXML Controller class
 *
 * @author edqu3
 */
public class ConfirmDialogController implements Initializable, Controller<ConfirmDialogParameters> {
    
    @FXML
    private Button yesButton;
    @FXML
    private Button noButton;
    @FXML
    private Label  messageLabel;
    
    private ConfirmDialogParameters parameters;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    @FXML
    private void closeDialog(ActionEvent event) {
        if (event.getSource() == yesButton) {
            DataManager.INSTANCE.removePlayer(parameters.getPlayer());
        }
        close();
    }

    @Override
    public void reset() {
        
    }

    @Override
    public void close() {
        ScreenManager.INSTANCE.getScreen(Screen.CONFIRM_DIALOG).getStage().close();
    }

    @Override
    public void setParameters(ConfirmDialogParameters parameters) {
        this.parameters = parameters;
        messageLabel.setText(parameters.getMessage());
        noButton.setText(parameters.getNoButtonText());
        yesButton.setText(parameters.getYesButtonText());
        
    }
    
}
