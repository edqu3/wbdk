package wbdk.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import wbdk.Controller;
import wbdk.DialogParameters;
import wbdk.data.FantasyTeam;

/**
 * works
 *
 * @author edqu3
 */
public class FantasyTeamDialogController implements Initializable, Controller{
    @FXML
    private Button completeButton;
    @FXML
    private TextField teamNameLabel;
    @FXML
    private TextField ownerNameLabel;
    @FXML
    private Button cancelButton;
    
    private FantasyTeam team = null;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    /**
     * mutate team field if this team is intended to be updated.
     * @param team 
     */
    public void setTeam(FantasyTeam team){
        
        teamNameLabel.setText(team.getTeamName().get());
        ownerNameLabel.setText(team.getOwner().get());
        
        this.team = team;
    }
    
    /**
     * called when user clicks complete or cancel buttons.
     * @param event 
     */
    @FXML
    private void closeDialog(ActionEvent event) {
        if ( event.getSource() == cancelButton) {
            ((Stage)completeButton.getScene().getWindow()).close();
        }else{
                    
            String owner = ownerNameLabel.getText();
            String fteam = teamNameLabel.getText();
            
            if (!owner.isEmpty() && !fteam.isEmpty()) {
                // true when the call comes from add button
                if (team == null) {
                    this.team = new FantasyTeam(owner, fteam);
                }
                else{
                    // true when editing.
                    team.setOwner(owner);
                    team.setTeamName(fteam);
                }
            }
            ((Stage)completeButton.getScene().getWindow()).close();
        }
    }
    
    /**
     * call ONLY after dialog box was closed.
     * @return 
     */
    public FantasyTeam getTeam(){
        return team;
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setParameters(DialogParameters parameters) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
