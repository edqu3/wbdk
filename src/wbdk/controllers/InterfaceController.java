package wbdk.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import wbdk.DataManager;
import wbdk.Controller;
import wbdk.DialogParameters;
import wbdk.MessageDialogParamaters;
import wbdk.Screen;
import wbdk.ScreenManager;

/**
 * This class takes care of changing screens.
 * @author edqu3
 */
public class InterfaceController implements Initializable, Controller{
    
    //<editor-fold defaultstate="collapsed" desc="controls">
    // Start Screen
    private Stage primaryStage;
    
    /// TOP TOOL BAR.
    @FXML
    private HBox topToolBar;
    @FXML
    private Button newButton;
    @FXML
    private Button openButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button exportButton;
    @FXML
    private Button exitButton;
    
    /* BorderPane containing the current Screen */
    @FXML
    private BorderPane currentScreenBorderContainer;
    
    /// BOTTOM TOOL BAR
    @FXML
    private HBox bottomToolBar;
    @FXML
    private Button teamsButton;
    @FXML
    private Button playersButton;
    @FXML
    private Button standingsButton;
    @FXML
    private Button draftButton;
    @FXML
    private Button mlbTeamsButton;
    //</editor-fold>
    
    /**
     * 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        DataManager.INSTANCE.loadPlayers();
        bottomToolBar.setVisible(false);
    }
    
    @FXML
    private void newDraft(){
        
        Screen teams_screen = ScreenManager.INSTANCE.getScreen(Screen.TEAMS_SCREEN);
        
        currentScreenBorderContainer.setCenter(teams_screen.getRoot());
        bottomToolBar.setVisible(true);
        
        // Display new draft message dialog
        ScreenManager.INSTANCE.showDialog(
                Screen.MESSAGE_DIALOG,
                new MessageDialogParamaters().setButtonText("OK").setMessage("New draft created but not saved."),
                getPrimaryStage());
    }
    
    @FXML
    private void loadDraft(ActionEvent event) {
                
    }

    @FXML
    private void saveDraft(ActionEvent event) {
    }

    @FXML
    private void exportDraft(ActionEvent event) {
    }

    @FXML
    private void exit(ActionEvent event) {
        close();
    }
    
    @FXML
    private void changeScene(ActionEvent event){
        
        Parent screen = null;
        
        if (event.getSource() == teamsButton) {
            screen = ScreenManager.INSTANCE.getScreen(Screen.TEAMS_SCREEN).getRoot();
        }
        else if (event.getSource() == playersButton){
            screen = ScreenManager.INSTANCE.getScreen(Screen.PLAYERS_SCREEN).getRoot();
        }
        else if (event.getSource() == standingsButton){
            screen = ScreenManager.INSTANCE.getScreen(Screen.STANDINGS_SCREEN).getRoot();
        }
        else if (event.getSource() == draftButton){
            screen = ScreenManager.INSTANCE.getScreen(Screen.DRAFT_SCREEN).getRoot();
        }
        else if (event.getSource() == mlbTeamsButton){
            screen = ScreenManager.INSTANCE.getScreen(Screen.MLBTEAMS_SCREEN).getRoot();
        }
        if (screen != null) {
            currentScreenBorderContainer.setCenter(screen);
        }
    }
    
    /**
     * Hacky way of getting a reference to the primary stage.
     * @return the outer most window.
     */
    private Stage getPrimaryStage(){
        return (Stage) ScreenManager.INSTANCE.getScreen(Screen.INTERFACE_SCREEN).getRoot().getScene().getWindow();
    }

    @Override
    public void reset() {
        
    }

    @Override
    public void setParameters(DialogParameters a) {
    }

    @Override
    public void close() {
        ScreenManager.INSTANCE.getScreen(Screen.INTERFACE_SCREEN).getStage().close();
    }

}