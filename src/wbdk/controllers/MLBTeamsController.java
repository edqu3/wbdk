package wbdk.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import wbdk.Controller;
import wbdk.DataManager;
import wbdk.DialogParameters;
import wbdk.data.GameProperties;
import wbdk.data.Player;

public class MLBTeamsController implements Initializable, Controller{
    
    //<editor-fold defaultstate="collapsed" desc="controls">
    @FXML
    private ComboBox<String>            mlbTeams;
    @FXML
    private TableView<Player>           playersTableView;
    @FXML
    private TableColumn<Player, String> firstNameColumn;
    @FXML
    private TableColumn<Player, String> lastNameColumn;
    @FXML
    private TableColumn<Player, String> positionColumn;
    //</editor-fold>
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        // Load MLB Teams.
        mlbTeams.setItems(
                FXCollections.observableArrayList(
                    Arrays.asList(GameProperties.MLB_TEAMS)
                )
        );
                
        initTable();
        
    }
    
    /**
     * returns all players matching mlbteam
     * @param players
     * @param mlbteam
     * @return 
     */
    private List<Player> getPlayersInMLBTeam(List<Player> players, String mlbteam){
        ArrayList<Player> newList = new ArrayList<>();
        for (Player player : players) {
            if (player.getTeamColumn().get().equals(mlbteam)) {
                newList.add(player);
            }
        }
        return newList;
    }
    
    /**
     * fires when a combo box item is selected.
     * @param event 
     */
    @FXML
    private void selectMLBTeam(ActionEvent event) {
        
        String selectedItem           = mlbTeams.getSelectionModel().getSelectedItem();
        List<Player> playersList      = DataManager.INSTANCE.getBackEndPlayerList();
        List<Player> playersInMLBTeam = getPlayersInMLBTeam(playersList, selectedItem);
        
        // update playerslist.
        playersTableView.getItems().setAll(playersInMLBTeam);
        
        // sort by last name A -> Z 
        lastNameColumn.setSortType(TableColumn.SortType.ASCENDING);
        playersTableView.getSortOrder().add(lastNameColumn);
        
    }

    private void initTable(){
        // set columns
        firstNameColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                        param.getValue().getFirstNameColumn());
        
        lastNameColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                        param.getValue().getLastNameColumn());

        positionColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                        param.getValue().getPositionsColumn());
        
        playersTableView.setItems(FXCollections.observableArrayList());        
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setParameters(DialogParameters a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}