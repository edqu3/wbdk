package wbdk.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import wbdk.Controller;
import wbdk.DataManager;
import wbdk.DialogParameters;
import wbdk.EditPlayerDialogParameters;
import wbdk.Properties;
import wbdk.Screen;
import wbdk.ScreenManager;
import wbdk.data.Player;
import wbdk.data.FantasyTeam;
import wbdk.data.GameProperties;

/**
 * FXML Controller class
 *
 * @author edqu3
 */
public class EditPlayerDialogController implements Initializable, Controller<EditPlayerDialogParameters>{
    
    //<editor-fold defaultstate="collapsed" desc="controls">
    @FXML
    private Button completeButton;
    @FXML
    private Button cancelButton;
    @FXML
    private ImageView portraitImageView;
    @FXML
    private ImageView flagImageView;
    @FXML
    private Label nameLabel;
    @FXML
    private Label positionLabel;
    @FXML
    private ComboBox<String> teamComboBox;
    @FXML
    private ComboBox<String> positionsComboBox;
    @FXML
    private ComboBox<String> contractComboBox;
    @FXML
    private TextField salaryTextField;
    //</editor-fold>

    private Player player;
    private final List<String> contractList = new ArrayList<>();
    
    private boolean requiredfieldCount;
    private boolean salaryvalid;
    private boolean teamvalid;
    private boolean contractvalid;
    private EditPlayerDialogParameters parameters;
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        init();
        
        teamComboBox.valueProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    teamvalid = true;
                }else{
                    teamvalid = false;
                }
                requiredfieldCount = salaryvalid && contractvalid && teamvalid;
                if (requiredfieldCount) {
                    completeButton.setDisable(false);
                }else{
                    completeButton.setDisable(true);
                
                }
            }
        });
        
        salaryTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                if (newValue.trim().length() > 0 && !newValue.startsWith("0")) {
                    salaryvalid = true;                       

                }else{
                    salaryvalid = false;
                }
                requiredfieldCount = salaryvalid && contractvalid && teamvalid;
                if (requiredfieldCount) {
                    completeButton.setDisable(false);
                    
                }else{
                    completeButton.setDisable(true);
                }                
            }
        });
        
        contractComboBox.valueProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    contractvalid = true;
                }else{
                    contractvalid = false;
                }
                requiredfieldCount = salaryvalid && contractvalid && teamvalid;
                if (requiredfieldCount) {
                    completeButton.setDisable(false);
                }else{
                    completeButton.setDisable(true);
                
                }
            }
        });
        
    }    
    
    private void init(){
        contractList.addAll(Arrays.asList(GameProperties.CONTRACT));
    }
    
    public Player getPlayer() {
        return player;
    }
    
    @FXML
    public void closeDialog(ActionEvent e){
        if (e.getSource() == completeButton) {
            // commmit edits
            updatePlayer();
        }
        close();
    }
    
    private Image getPlayerImage(String path){
        try {
            return new Image(path);
        } catch (Exception e) {
            return new Image(Properties.PLAYER_IMAGES_PATH + Properties.IMAGE_NOT_FOUND);
        }
    }
    
    private void updatePlayer(){
        // only these properties can be updated.
        player.setContract(contractComboBox.getSelectionModel().getSelectedItem());
        player.setSalary(Integer.parseInt(salaryTextField.getText()));
        player.setCurrentFantasyTeam(teamComboBox.getSelectionModel().getSelectedItem());
        player.setPlayingPosition(positionsComboBox.getSelectionModel().getSelectedItem());
        
        // a player whose team is chosen in this dialog must be moved to the Fantasy Teams table.
        // how?
        DataManager.INSTANCE.updatePlayerInTeam(player);
    }
    
    private void loadPlayer(Player player) {
        
        String firstName   = player.getFirstNameColumn().get();
        String lastName    = player.getLastNameColumn().get();
        String nation      = player.getNationOfBirthColumn().get();
        String positions   = player.getPositionsColumn().get();
        String salary      = String.valueOf(player.getSalary().get());
        String contract    = player.getContract().get();
        String playingPos  = player.getPlayingPosition().get();
        String currentTeam = player.getCurrentFantasyTeam().get();
        
        nameLabel.setText(firstName + " " + lastName);
        
        // Load player image if its available.
        portraitImageView.setImage(
                getPlayerImage(Properties.PLAYER_IMAGES_PATH+lastName+firstName+Properties.JPG_FILE_EXT));
        
        flagImageView.setImage(new Image(Properties.FLAG_IMAGES_PATH + nation + Properties.PNG_FILE_EXT));
        positionLabel.setText(positions);
        // fantasy team comes from the list of teams user created in the fantasy teams screen
        
        // combo box init
        List<String> teams = new ArrayList();
        for (FantasyTeam t : DataManager.INSTANCE.getTeams()) { 
            // add all fantasy teams to combobox
            teams.add(t.getTeamName().get()); 
        }
        teamComboBox.setItems(FXCollections.observableArrayList(teams));
        teamComboBox.getSelectionModel().select(currentTeam);
        
        // position
        List<String> eligiblePositions = Player.parsePlayerPosition(player);
        positionsComboBox.setItems(FXCollections.observableArrayList(eligiblePositions));
        positionsComboBox.getSelectionModel().select(playingPos);
        
        // Contracts, values never change.
        contractComboBox.setItems(FXCollections.observableArrayList(contractList));
        contractComboBox.getSelectionModel().select(contract);
        
        // Salary
        salaryTextField.setText(salary);
        
    }

    @Override
    public void reset() {
    }

    @Override
    public void close() {
        ScreenManager.INSTANCE.getScreen(Screen.EDIT_PLAYER_DIALOG).getStage().close();
    }

    @Override
    public void setParameters(EditPlayerDialogParameters parameters) {
        this.parameters = parameters;
        loadPlayer(parameters.getPlayer());
    }
    
}
