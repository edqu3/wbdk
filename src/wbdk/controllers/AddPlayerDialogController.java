package wbdk.controllers;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import wbdk.AddPlayerDialogParameters;
import wbdk.Controller;
import wbdk.DataManager;
import wbdk.Screen;
import wbdk.ScreenManager;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;
import wbdk.data.Player;
import wbdk.data.GameProperties;

/**
 * FXML Controller class
 *
 * @author edqu3
 */
public class AddPlayerDialogController implements Initializable, Controller<AddPlayerDialogParameters> {
    
    //<editor-fold defaultstate="collapsed" desc="controls">
    @FXML
    private Button completeButton;
    @FXML
    private TextField firstNameTextField;
    @FXML
    private TextField lastNameTextField;
    @FXML
    private Button cancelButton;
    @FXML
    private CheckBox catcherCheckBox;
    @FXML
    private CheckBox firstBasemanCheckBox;
    @FXML
    private CheckBox thirdBasemanCheckBox;
    @FXML
    private CheckBox secondBasemanCheckBox;
    @FXML
    private CheckBox shortStopCheckBox;
    @FXML
    private CheckBox outfielderCheckBox;
    @FXML
    private CheckBox pitcherCheckbox;
    @FXML
    private ComboBox<String> teamComboBox;
    //</editor-fold>

    // TODO: These variables are also used when reading or writing a json file,
    // they belong to GameProperties.
    private final static String SEPARATOR  = "_";
    private final static String CATCHER    = "C";
    private final static String FBMAN      = "1B";
    private final static String SBMAN      = "2B";
    private final static String TBMAN      = "3B";
    private final static String SHORTSTOP  = "SS";
    private final static String OUTFIELDER = "OF";
    private final static String PITCHER    = "P";
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        init();
    }
    
    private void init(){
        
        teamComboBox.setItems(
                FXCollections.observableArrayList(
                    Arrays.asList(GameProperties.MLB_TEAMS)
                )
        );
        
    }
    
    private void getData(){
        
        String  fName       = firstNameTextField.getText();
        String  lName       = lastNameTextField.getText();
        String  team        = teamComboBox.getSelectionModel().getSelectedItem();
        boolean catcher     = catcherCheckBox.isSelected();
        boolean fBman       = firstBasemanCheckBox.isSelected();
        boolean sBman       = secondBasemanCheckBox.isSelected();
        boolean tBman       = thirdBasemanCheckBox.isSelected();
        boolean shortstop   = shortStopCheckBox.isSelected();
        boolean outfielder  = outfielderCheckBox.isSelected();
        boolean pitcher     = pitcherCheckbox.isSelected();
        
        Player player = new Hitter();

        if ( pitcher && ((catcher || fBman || sBman || tBman || shortstop || outfielder) == false)) {
            player = new Pitcher();
        }
        else if (catcher || fBman || sBman || tBman || shortstop || outfielder) {
            player = new Hitter();
        }
        
        player.setFirstName(fName);
        player.setLastName(lName);
        player.setTeamName(team);
        
        StringBuilder sb = new StringBuilder();
        
        if (catcher) {
            sb.append(CATCHER).append(SEPARATOR);
        }
        if (fBman) {
            sb.append(FBMAN).append(SEPARATOR);
        }        
        if (sBman) {
            sb.append(SBMAN).append(SEPARATOR);
        }        
        if (tBman) {
            sb.append(TBMAN).append(SEPARATOR);
        }        
        if (shortstop) {
            sb.append(SHORTSTOP).append(SEPARATOR);
        }        
        if (outfielder) {
            sb.append(OUTFIELDER).append(SEPARATOR);
        }        
        if (pitcher) {
            sb.append(PITCHER);
        }
        
        // remove leading underscores if necessary
        player.setQualifyingPositions(
            (sb.toString().endsWith(SEPARATOR)) ? sb.toString().substring(0, sb.length() - 1) : sb.toString()
        );
        
        // finally add player to the back end player list.
        DataManager.INSTANCE.addPlayerToPool(player);
    }

    @FXML
    public void cancel(){
        // Discard all data
        close();
    }
    
    @FXML
    public void complete(ActionEvent e){
        // Put data inside the DataManager.
        // The data manager will update (add/remove/edit) the backend playersList.
        getData();
        close();
    }

    @Override
    public void reset() {
        // reset anything set by setParameters()
    }

    @Override
    public void close() {
        ScreenManager.INSTANCE.getScreen(Screen.ADD_PLAYER_DIALOG).getStage().close();
    }

    @Override
    public void setParameters(AddPlayerDialogParameters parameters) {
        // custom labels / control properties here.
    }
    
}