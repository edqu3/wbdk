package wbdk.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import wbdk.Controller;
import wbdk.DialogParameters;
import wbdk.data.Player;


public class DraftController implements Initializable, Controller{
    //<editor-fold defaultstate="collapsed" desc="controls">
    @FXML
    private Button draftOneButton;
    @FXML
    private Button autoDraftButton;
    @FXML
    private Button pauseDraftButton;
    @FXML
    private TableView<Player>                draftTableView;
    @FXML
    private TableColumn<Player, Number>      pickColumn;
    @FXML
    private TableColumn<Player, String>      firstNameColumn;
    @FXML
    private TableColumn<Player, String>      lastNameColumn;
    @FXML
    private TableColumn<Player, String>      teamColumn;
    @FXML
    private TableColumn<Player, String>      contractColumn;
    @FXML
    private TableColumn<Player, Number>      salaryColumn;      // no cents
    //</editor-fold>
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initTable();
        
    }

    private void initTable(){
        setColumns();
        
        // get list of players who are already drafted.
        // put them in table.
        
        
    }
    
    private void setColumns(){
        // Team column comes from team.
        pickColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) -> 
                    param.getValue().getPickNumber());

        firstNameColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                    param.getValue().getFirstNameColumn());
        
        lastNameColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                    param.getValue().getLastNameColumn());

        teamColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                    param.getValue().getTeamColumn());

        contractColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, String> param) -> 
                    param.getValue().getContract()); 
        
        salaryColumn.setCellValueFactory(
                (TableColumn.CellDataFeatures<Player, Number> param) ->
                    param.getValue().getSalary());
        
    }
    
    @FXML
    private void pauseDraft(ActionEvent event) {
        
    }
    
    @FXML
    private void draftPlayer(ActionEvent event){
        // call methods that add a player from the player pool to a fantasy team.
        
        // grab player from the DataManager and put it in table.
        
    }

    @FXML
    private void autoDraft(ActionEvent event) {
        
    }

    @Override
    public void reset() {
    }
    
    @Override
    public void setParameters(DialogParameters a) {
    }

    @Override
    public void close() {
    }


}