package wbdk.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import wbdk.Controller;
import wbdk.MessageDialogParamaters;
import wbdk.Screen;
import wbdk.ScreenManager;

/**
 * FXML Controller class
 *
 * @author edqu3
 */
public class MessageDialogController implements Initializable, Controller<MessageDialogParamaters> {
    
    //<editor-fold defaultstate="collapsed" desc="controls">
    @FXML
    private Label messageLabel;
    @FXML
    private Button closeDialogButton;
    //</editor-fold>

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    @FXML
    public void closeDialog(){
        close();
    }
    
    @Override
    public void reset() {
        messageLabel.setText(null);
        closeDialogButton.setText(null);
    }

    @Override
    public void setParameters(MessageDialogParamaters parameters) {        
        messageLabel.setText(parameters.getMessage());
        closeDialogButton.setText(parameters.getButtonText());
    }

    @Override
    public void close() {
        ((Stage) ScreenManager.INSTANCE.getScreen(
                Screen.MESSAGE_DIALOG)
                .getRoot()
                .getScene()
                .getWindow())
                .close();
    }

    
}
