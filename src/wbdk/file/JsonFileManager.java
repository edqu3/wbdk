package wbdk.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.*;
import wbdk.data.Player;
import java.util.List;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;

/**
 * This class reads json files.
 * @author edqu3
 */
public class JsonFileManager {
    
    private final String PATH;
    
    private JsonFileManager() {PATH = "";}

    public JsonFileManager(String path) {
        PATH = path;
    }
    
    // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJsonFile() {
        InputStream is = null;
        
        try 
        {
            is = new FileInputStream(PATH);
            JsonReader jsonReader = Json.createReader(is);
            JsonObject json = jsonReader.readObject();
            jsonReader.close();
            return json;
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(JsonFileManager.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally 
        {
            try 
            {
                is.close();
            } 
            catch (IOException ex)
            {
                Logger.getLogger(JsonFileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public static String cleanJsonValue(JsonValue value){
        return value.toString().replace('\"', ' ').trim();
    }
    
    public static int parseJsonIntegerValue(JsonValue value){
        return Integer.parseInt(cleanJsonValue(value));
    }

    public static double parseJsonDoubleValue(JsonValue value){
        return Double.parseDouble(cleanJsonValue(value));
    }
    
    public Player createHitter(JsonObject o){
        Hitter hitter = new Hitter();
        
        hitter.setAtBats(JsonFileManager.parseJsonDoubleValue(o.get(Hitter.AT_BATS)));
        hitter.setRuns(JsonFileManager.parseJsonIntegerValue(o.get(Hitter.RUNS)));
        hitter.setHomeRuns(JsonFileManager.parseJsonIntegerValue(o.get(Hitter.HOME_RUNS)));
        hitter.setRunsBattedIn(JsonFileManager.parseJsonIntegerValue(o.get(Hitter.RUNS_BATTED_IN)));
        hitter.setStolenBases(JsonFileManager.parseJsonIntegerValue(o.get(Hitter.STOLEN_BASES)));
        
        hitter.setFirstName(JsonFileManager.cleanJsonValue(o.get(Player.FIRST_NAME)));
        hitter.setLastName(JsonFileManager.cleanJsonValue(o.get(Player.LAST_NAME)));
        hitter.setTeamName(JsonFileManager.cleanJsonValue(o.get(Player.TEAM))); 
        hitter.setHits(JsonFileManager.parseJsonIntegerValue(o.get(Player.HITS)));
        hitter.setQualifyingPositions(JsonFileManager.cleanJsonValue(o.get(Player.QUALIFYING_POSITION)));
        hitter.setNotes(JsonFileManager.cleanJsonValue(o.get(Player.NOTES)));
        hitter.setYearOfBirth(JsonFileManager.parseJsonIntegerValue(o.get(Player.YEAR_OF_BIRTH)));
        hitter.setNationOfBirth(JsonFileManager.cleanJsonValue(o.get(Player.NATION_OF_BIRTH)));
        
        return hitter;
    }
    
    
    public Player createPitcher(JsonObject o){
        Pitcher pitcher = new Pitcher();
        
        pitcher.setInningsPitched(JsonFileManager.parseJsonDoubleValue(o.get(Pitcher.INNINGS_PITCHED)));
        pitcher.setEarnedRuns(JsonFileManager.parseJsonIntegerValue(o.get(Pitcher.EARNED_RUNS)));
        pitcher.setBasesOnBalls(JsonFileManager.parseJsonIntegerValue(o.get(Pitcher.BASES_ON_BALLS)));
        pitcher.setHits(JsonFileManager.parseJsonIntegerValue(o.get(Player.HITS)));
        
        pitcher.setFirstName(JsonFileManager.cleanJsonValue(o.get(Player.FIRST_NAME)));
        pitcher.setLastName(JsonFileManager.cleanJsonValue(o.get(Player.LAST_NAME)));
        pitcher.setTeamName(JsonFileManager.cleanJsonValue(o.get(Player.TEAM))); 
        pitcher.setNotes(JsonFileManager.cleanJsonValue(o.get(Player.NOTES)));
        pitcher.setQualifyingPositions("P");
        pitcher.setYearOfBirth(JsonFileManager.parseJsonIntegerValue(o.get(Player.YEAR_OF_BIRTH)));
        pitcher.setNationOfBirth(JsonFileManager.cleanJsonValue(o.get(Player.NATION_OF_BIRTH)));
        pitcher.setWalks(JsonFileManager.parseJsonIntegerValue(o.get(Pitcher.WALKS)));
        pitcher.setSaves(JsonFileManager.parseJsonIntegerValue(o.get(Pitcher.SAVES))); 
        pitcher.setStrikeOuts(JsonFileManager.parseJsonIntegerValue(o.get(Pitcher.STRIKE_OUTS)));
        
        return pitcher;
    }
    
    /**
     *
     * @param arrayKey 
     * @return
     */
    public List<Player> getPitchers(String arrayKey) {
        JsonObject jsonObject = loadJsonFile();
        JsonArray  jsonArray  = jsonObject.getJsonArray(arrayKey);              // create constant for this string
        
        List<Player> players = new ArrayList();
        for (int i = 0; i < jsonArray.size(); i++) {
            players.add(createPitcher(jsonArray.getJsonObject(i)));
        }
        
        return players;
    }
    
    public List<Player> getHitters(String arrayKey) {
        JsonObject jsonObject = loadJsonFile();
        JsonArray  jsonArray  = jsonObject.getJsonArray(arrayKey);              // create constant for this string
        
        List<Player> players = new ArrayList();
        for (int i = 0; i < jsonArray.size(); i++) {
            players.add(createHitter(jsonArray.getJsonObject(i)));
        }
        
        return players;
    }
}
