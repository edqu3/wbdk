package wbdk;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Players implementing this class must provide their specific data (Hitters/Pitcher) 
 * to be used in a Table Column.
 * @author edqu3
 */
public interface ColumnData {
    
    SimpleStringProperty  getFirstNameColumn();
    SimpleStringProperty  getLastNameColumn();
    SimpleStringProperty  getTeamColumn();
    SimpleStringProperty  getPositionsColumn();
    SimpleIntegerProperty getYearOfBirthColumn();
    SimpleIntegerProperty getR_WColumn();
    SimpleIntegerProperty getHR_SVColumn();
    SimpleIntegerProperty getRBI_KColumn();
    SimpleDoubleProperty  getSB_ERAColumn();
    SimpleDoubleProperty  getBA_WHIPColumn();
    SimpleIntegerProperty getEstimatedValueColumn();
    SimpleStringProperty  getNotesColumn();
    SimpleStringProperty  getNationOfBirthColumn();
}
