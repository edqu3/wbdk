package wbdk;

/**
 * Type identifier for all controllers.
 * @author edqu3
 * @param <T>
 */
public interface Controller<T extends DialogParameters> {
    /**
     *  resets all parameters.
     */
    public void reset();
    
    public void close();
    /**
     * Pass in parameters to controller.
     * @param parameters 
     */
    public void setParameters(T parameters);
    
    

}