package wbdk.data;

import java.util.Arrays;
import java.util.List;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import wbdk.ColumnData;

/**
 *
 * @author edqu3
 */
public abstract class Player implements ColumnData{
    private final static String QUALIFYING_POSITION_SEPARATOR = "_";
    
    // Player Statistics
    public final static String TEAM                = "TEAM";
    public final static String LAST_NAME           = "LAST_NAME";
    public final static String FIRST_NAME          = "FIRST_NAME";
    public final static String HITS                = "H";
    public final static String NOTES               = "NOTES";
    public final static String YEAR_OF_BIRTH       = "YEAR_OF_BIRTH";
    public final static String NATION_OF_BIRTH     = "NATION_OF_BIRTH";
    public final static String QUALIFYING_POSITION = "QP";

    // TABLE COLUMN PROPERTIES
    public Player(){
        this.firstName           = new SimpleStringProperty();
        this.lastName            = new SimpleStringProperty();
        this.teamName            = new SimpleStringProperty();
        this.notes               = new SimpleStringProperty();
        this.yearOfBirth         = new SimpleIntegerProperty();
        this.nationOfBirth       = new SimpleStringProperty();
        this.estimatedValue      = new SimpleIntegerProperty();
        this.qualifyingPositions = new SimpleStringProperty();
        this.currentFantasyTeam  = new SimpleStringProperty();
        this.playingPosition     = new SimpleStringProperty();
        this.contract            = new SimpleStringProperty();
        this.salary              = new SimpleIntegerProperty();
        this.pickNumber          = new SimpleIntegerProperty();
    }

    //<editor-fold defaultstate="collapsed" desc="fields used by the fantasy teams screen.">
    private final SimpleStringProperty currentFantasyTeam;
    private final SimpleStringProperty playingPosition;
    private final SimpleStringProperty contract;
    private final SimpleIntegerProperty salary;

    public void setCurrentFantasyTeam(String team) {
        this.currentFantasyTeam.set(team);
    }
    
    public void setPlayingPosition(String pos) {
        this.playingPosition.set(pos);
    }
    
    public void setContract(String contract) {
        this.contract.set(contract);
    }
    
    public void setSalary(int salary) {
        this.salary.set(salary);
    }
    
    public SimpleStringProperty getCurrentFantasyTeam() {
        return currentFantasyTeam;
    }

    public SimpleStringProperty getPlayingPosition() {
        return playingPosition;
    }

    public SimpleStringProperty getContract() {
        return contract;
    }

    public SimpleIntegerProperty getSalary() {
        return salary;
    }
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Player specific setters">
    private final SimpleStringProperty  firstName;
    private final SimpleStringProperty  lastName;
    private final SimpleStringProperty  teamName;
    private final SimpleStringProperty  qualifyingPositions;
    private final SimpleStringProperty  notes;
    private final SimpleIntegerProperty yearOfBirth;
    private final SimpleStringProperty  nationOfBirth;
    // no data given for this yet, but it does show up on the table.
    private final SimpleIntegerProperty estimatedValue;
    // not in table
    protected double hits;                               
    
    public double getHits() {
        return hits;
    }

    public void setHits(double hits) {
        this.hits = hits;
    }
    
    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }
    
    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }
    
    public void setTeamName(String teamName) {
        this.teamName.set(teamName);
    }
    
    public void setNotes(String notes) {
        this.notes.set(notes);
    }
    
    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth.set(yearOfBirth);
    }
    
    public void setNationOfBirth(String nationOfBirth) {
        this.nationOfBirth.set(nationOfBirth);
    }
    
    public void setEstimatedValue(int estimatedValue) {
        this.estimatedValue.set(estimatedValue);
    }
    
    public void setQualifyingPositions(String qualifyingPositions) {
        this.qualifyingPositions.set(qualifyingPositions);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Table Column Properties">
    @Override
    public SimpleStringProperty getFirstNameColumn() {
        return firstName;
    }
    
    @Override
    public SimpleStringProperty getLastNameColumn() {
        return lastName;
    }
    
    @Override
    public SimpleStringProperty getTeamColumn() {
        return teamName;
    }
    
    @Override
    public SimpleStringProperty getPositionsColumn() {
        return qualifyingPositions;
    }
    
    @Override
    public SimpleStringProperty getNotesColumn() {
        return notes;
    }
    
    @Override
    public SimpleIntegerProperty getYearOfBirthColumn() {
        return yearOfBirth;
    }
    
    @Override
    public SimpleIntegerProperty getEstimatedValueColumn() {
        return estimatedValue;
    }
    
    @Override
    public SimpleStringProperty getNationOfBirthColumn(){
        return nationOfBirth;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Draft Screen properties">
    private final SimpleIntegerProperty pickNumber;
    
    public void setPickNumber(int pick){
        this.pickNumber.set(pick);
    }
    
    public SimpleIntegerProperty getPickNumber() {
        return pickNumber;
    }
    //</editor-fold>
 
    /**
     * returns all eligible positions this player can play.
     * @param item 
     * @return  
     */
    public static List<String> parsePlayerPosition(Player item) {
        String positionsString = item.qualifyingPositions.get();
        String[] split = positionsString.split(QUALIFYING_POSITION_SEPARATOR);
        return Arrays.asList(split);
    }
        
}
