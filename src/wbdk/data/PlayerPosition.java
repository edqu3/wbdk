package wbdk.data;

/**
 *
 * @author edqu3
 */
public enum PlayerPosition{
    PITCHER,
    OUTFIELDER,
    CATCHER,
    FIRST_BASE,
    SECOND_BASE,
    THIRD_BASE,
    CORNER_INFIELDER,
    MIDDLE_INFIELDER,
    SHORTSTOP,
    UTILITY;
}
