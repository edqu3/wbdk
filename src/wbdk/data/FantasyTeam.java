    package wbdk.data;

import java.util.HashMap;
import java.util.List;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * A list of Players.
 * @author edqu3
 */
public class FantasyTeam {
        
    private final ObservableList<Player> taxiSquad;
    private final ObservableList<Player> startingLineUp;
    private final HashMap<PlayerPosition, Integer> positions;
    
    private final SimpleIntegerProperty playerLimit;
    
    //<editor-fold defaultstate="collapsed" desc="Defaults">
    private final static int PLAYER_LIMIT         = 23;
    private final static int FUNDS                = 260;
    private final static int PITCHER_LIMIT        = 9;
    private final static int OUTFIELDER_LIMIT     = 5;
    private final static int CATCHER_LIMIT        = 2;
    private final static int FIRST_BASE_LIMIT     = 1;
    private final static int SECOND_BASE_LIMIT    = 1;
    private final static int THIRD_BASE_LIMIT     = 1;
    private final static int CORNER_INFIELD_LIMIT = 1;
    private final static int MIDDLE_INFIELD_LIMIT = 1;
    private final static int SHORTSTOP_LIMIT      = 1;
    private final static int UTILITY_LIMIT        = 1;
    //</editor-fold>

    private final SimpleStringProperty  owner;
    private final SimpleStringProperty  teamName;
    private final SimpleIntegerProperty playerNeeded;
    private final SimpleIntegerProperty funds;
    private final SimpleIntegerProperty pricePerPlayer;
    private final SimpleIntegerProperty runs;
    private final SimpleIntegerProperty homeRuns;
    private final SimpleIntegerProperty runsBattedIn;
    private final SimpleDoubleProperty  battingAverage;
    private final SimpleIntegerProperty wins;
    private final SimpleIntegerProperty strikeOuts;
    private final SimpleIntegerProperty saves;
    private final SimpleDoubleProperty  era;
    private final SimpleDoubleProperty  whip;
    private final SimpleIntegerProperty points;
    private final SimpleIntegerProperty stolenBases;
    
    
    public FantasyTeam(String owner, String teamName) {
        
        this.owner          = new SimpleStringProperty(owner);
        this.teamName       = new SimpleStringProperty(teamName);
        this.playerLimit    = new SimpleIntegerProperty(PLAYER_LIMIT);
        this.playerNeeded   = new SimpleIntegerProperty();
        this.funds          = new SimpleIntegerProperty(FUNDS);
        this.pricePerPlayer = new SimpleIntegerProperty();
        this.runs           = new SimpleIntegerProperty();
        this.homeRuns       = new SimpleIntegerProperty();
        this.runsBattedIn   = new SimpleIntegerProperty();
        this.battingAverage = new SimpleDoubleProperty();
        this.wins           = new SimpleIntegerProperty();
        this.strikeOuts     = new SimpleIntegerProperty();
        this.saves          = new SimpleIntegerProperty();
        this.era            = new SimpleDoubleProperty();
        this.whip           = new SimpleDoubleProperty();
        this.points         = new SimpleIntegerProperty();
        this.stolenBases    = new SimpleIntegerProperty();
        
        taxiSquad        = FXCollections.observableArrayList();
        startingLineUp   = FXCollections.observableArrayList();
        positions        = new HashMap<>();
        
        initPlayerPositionsCapacity();
    }
    
    private void initPlayerPositionsCapacity(){
        positions.put(
                PlayerPosition.PITCHER, PITCHER_LIMIT);
        positions.put(
                PlayerPosition.OUTFIELDER, OUTFIELDER_LIMIT);
        positions.put(
                PlayerPosition.CATCHER, CATCHER_LIMIT);
        positions.put(
                PlayerPosition.FIRST_BASE, FIRST_BASE_LIMIT);
        positions.put(
                PlayerPosition.SECOND_BASE, SECOND_BASE_LIMIT);
        positions.put(
                PlayerPosition.THIRD_BASE, THIRD_BASE_LIMIT);
        positions.put(
                PlayerPosition.CORNER_INFIELDER, CORNER_INFIELD_LIMIT);
        positions.put(
                PlayerPosition.MIDDLE_INFIELDER, MIDDLE_INFIELD_LIMIT);
        positions.put(
                PlayerPosition.SHORTSTOP, SHORTSTOP_LIMIT);
        positions.put(
                PlayerPosition.UTILITY, UTILITY_LIMIT);
    }

    public List<Player> getPlayers() {
        return startingLineUp;
    }
    
    public void setOwner(String owner) {
        this.owner.set(owner);
    }

    public void setTeamName(String teamName) {
        this.teamName.set(teamName);
    }

    public void addPlayer(Player p, PlayerPosition pos){
        // check if pos has available slot.
            // if available -> startingLineUp
            // deduct funds and add player
        // if full -> taxi squad

        if ((playerLimit.get() -1) >= 0 && (funds.get() /* - cost */ >= 0) ) {
            // deduct funds
            // TODO: player cost calculation
            int _funds = funds.get();
            funds.set(_funds--);
            // take spot
            positions.put(pos, (positions.get(pos) - 1) );
            // add player to team
            startingLineUp.add(p);
            int _playerLimit = playerLimit.get();
            playerLimit.set(_playerLimit--);
            // assign Pick number
            // TODO
        }
    }
    
    public void removePlayer(Player p, PlayerPosition pos){

        if (startingLineUp.contains(p)) {
            // return funds
            // TODO: player cost calculation
            int _funds = funds.get();
            funds.set(_funds++);
            // release spot
            positions.put(pos, (positions.get(pos) + 1));
            // release player from team
            startingLineUp.remove(p);
            int _playerLimit = playerLimit.get();
            playerLimit.set(_playerLimit++);
            // release pick number
            // TODO
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Team Properties">
    
    public SimpleStringProperty getOwner() {
        return owner;
    }

    public SimpleStringProperty getTeamName() {
        return teamName;
    }
    
    
    public SimpleIntegerProperty getPlayerNeeded() {
        return playerNeeded;
    }

    public SimpleIntegerProperty getFunds() {
        return funds;
    }

    public SimpleIntegerProperty getPricePerPlayer() {
        return pricePerPlayer;
    }

    public SimpleIntegerProperty getRuns() {
        return runs;
    }

    public SimpleIntegerProperty getHomeRuns() {
        return homeRuns;
    }

    public SimpleIntegerProperty getRunsBattedIn() {
        return runsBattedIn;
    }

    public SimpleDoubleProperty getBattingAverage() {
        
        return battingAverage;
    }

    public SimpleIntegerProperty getWins() {
        return wins;
    }

    public SimpleIntegerProperty getStrikeOuts() {
        return strikeOuts;
    }

    public SimpleIntegerProperty getSaves() {
        return saves;
    }

    public SimpleDoubleProperty getEra() {
        return era;
    }

    public SimpleDoubleProperty getWhip() {
        return whip;
    }

    public SimpleIntegerProperty getPoints() {
        return points;
    }

    public SimpleIntegerProperty getStolenBases() {
        return stolenBases;
    }
    
    //</editor-fold>

//    public double getBattingAverage() {
//        double hits   = 2.21;
//        double atBats = 3.21;
//        double val = (hits/atBats * 1000) / 1000;
//        return (Double.isNaN(val)) ? 0 : new BigDecimal(val).setScale(2, RoundingMode.HALF_UP).doubleValue();
//    }
//
//
//    public double era() {
//        double earnedRuns     = 1.2;
//        double inningsPitched = 2.0;
//        double val = Math.floor((earnedRuns*9/inningsPitched) * 1000) / 1000; 
//        return (Double.isNaN(val)) ? 0 : new BigDecimal(val).setScale(2, RoundingMode.HALF_UP).doubleValue();
//    }
//
//    public double whip() {
//        // calculate weighted whip
//        double walks          = 1.2;
//        double hits           = 2.2;
//        double inningsPitched = 2.0;
//        
//        double val = Math.floor(walks + (hits/inningsPitched) * 1000) / 1000;
//        return (Double.isNaN(val)) ? 0 : new BigDecimal(val).setScale(2, RoundingMode.HALF_UP).doubleValue();
//    }
    
}