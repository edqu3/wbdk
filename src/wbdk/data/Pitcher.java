package wbdk.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author edqu3
 */
public class Pitcher extends Player{
    
    // Pitcher Stats Constants
    public final static String INNINGS_PITCHED     = "IP";
    public final static String EARNED_RUNS         = "ER";
    public final static String WALKS               = "W";
    public final static String SAVES               = "SV";
    public final static String BASES_ON_BALLS      = "BB";                      // also known as Walks
    public final static String STRIKE_OUTS         = "K";
    
    // stats NOT TRACKED in table
    double inningsPitched;
    double earnedRuns;
    double basesOnBalls;
    
    // stats NOT TRACKED in table
    SimpleIntegerProperty walks;
    SimpleIntegerProperty saves;
    SimpleIntegerProperty strikeOuts;
    SimpleDoubleProperty  earnedRunsAverage;
    SimpleDoubleProperty  whip;

    public Pitcher(){
        this.walks = new SimpleIntegerProperty();
        this.saves = new SimpleIntegerProperty();
        this.strikeOuts = new SimpleIntegerProperty();
        this.earnedRunsAverage = new SimpleDoubleProperty();
        this.whip = new SimpleDoubleProperty();
    }

    public double getInningsPitched() {
        return inningsPitched;
    }

    public void setInningsPitched(double inningsPitched) {
        this.inningsPitched = inningsPitched;
    }

    public double getEarnedRuns() {
        return earnedRuns;
    }

    public void setEarnedRuns(double earnedRuns) {
        this.earnedRuns = earnedRuns;
    }

    public double getBasesOnBalls() {
        return basesOnBalls;
    }

    public void setBasesOnBalls(double basesOnBalls) {
        this.basesOnBalls = basesOnBalls;
    }
    
    private double getWHIP(){
        double val = Math.floor(walks.get() + (hits/inningsPitched) * 1000) / 1000;
        return (Double.isNaN(val)) ? 0 : new BigDecimal(val).setScale(3, RoundingMode.HALF_UP).doubleValue();
    }
    
    private double getERA(){
        double val = Math.floor((earnedRuns*9/inningsPitched) * 1000) / 1000; 
        return (Double.isNaN(val)) ? 0 : new BigDecimal(val).setScale(3, RoundingMode.HALF_UP).doubleValue();
    }
    
    public SimpleIntegerProperty walksProperty(){
        return walks;
    }
    
    public int getWalks(){
        return walks.get();
    }

    public void setWalks(int newVal){
        walks.set(newVal);
    }    
    
    
    public SimpleIntegerProperty savesProperty(){
        return saves;
    }
    
    public int getSaves(){
        return saves.get();
    }

    public void setSaves(int newVal){
        saves.set(newVal);
    }    

    
    public SimpleIntegerProperty strikeOutsProperty(){
        return strikeOuts;
    }
    
    public int getStrikeOuts(){
        return strikeOuts.get();
    }

    public void setStrikeOuts(int newVal){
        strikeOuts.set(newVal);
    }

    
    public SimpleDoubleProperty earnedRunsAverageProperty(){
        setEarnedRunsAverageProperty(getERA());
        return earnedRunsAverage;
    }
    
    public double getEarnedRunsAverageProperty(){
        return earnedRunsAverage.get();
    }
    
    public void setEarnedRunsAverageProperty(double newVal){
        earnedRunsAverage.set(newVal);
    }
    
    
    public SimpleDoubleProperty whipProperty(){
        setWhipProperty(getWHIP());
        return whip;
    }
    
    public double getWhipProperty(){
        return whip.get();
    }
    
    public void setWhipProperty(double newVal){
        whip.set(newVal);
    }

    @Override
    public SimpleIntegerProperty getR_WColumn() {
        return walks;
    }

    @Override
    public SimpleIntegerProperty getHR_SVColumn() {
        return saves;
    }

    @Override
    public SimpleIntegerProperty getRBI_KColumn() {
        return strikeOuts;
    }

    @Override
    public SimpleDoubleProperty getSB_ERAColumn() {
        setEarnedRunsAverageProperty(getERA());
        return earnedRunsAverage;
    }

    @Override
    public SimpleDoubleProperty getBA_WHIPColumn() {
        setWhipProperty(getWHIP());
        return whip;
    }
   
}
