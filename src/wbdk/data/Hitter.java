package wbdk.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author edqu3
 */
public class Hitter extends Player{

    // Hitter Stats
    public final static String AT_BATS             = "AB";
    public final static String RUNS                = "R";
    public final static String HOME_RUNS           = "HR";
    public final static String RUNS_BATTED_IN      = "RBI";
    public final static String STOLEN_BASES        = "SB";

    double atBats;
    SimpleIntegerProperty runs;
    SimpleIntegerProperty homeRuns;
    SimpleIntegerProperty runsBattedIn;
    SimpleDoubleProperty  stolenBases;
    SimpleDoubleProperty  battingAverage;

    public Hitter(){
        this.runs           = new SimpleIntegerProperty();
        this.homeRuns       = new SimpleIntegerProperty();
        this.runsBattedIn   = new SimpleIntegerProperty();
        this.stolenBases    = new SimpleDoubleProperty();
        this.battingAverage = new SimpleDoubleProperty();
    }
    
    public SimpleDoubleProperty battingAverageProperty(){
        battingAverage.set(getBattingAverage());
        return battingAverage;
    }
    
    public double getBattingAverageProperty(){
        return battingAverage.get();
    }
    
    public void setBattingAverageProperty(double newVal){
        battingAverage.set(newVal);
    }    
    
    public SimpleIntegerProperty runsProperty(){
        return runs;
    }
    
    public int getRuns(){
        return runs.get();
    }
    
    public void setRuns(int newVal){
        runs.set(newVal);
    }    
    
    public SimpleIntegerProperty homeRunsProperty(){
        return homeRuns;
    }
    
    public int getHomeRuns(){
        return homeRuns.get();
    }
    
    public void setHomeRunsProperty(int newVal){
        homeRuns.set(newVal);
    }
    
    public SimpleIntegerProperty runsBattedInProperty(){
        return runsBattedIn;
    }
    
    public int getRunsBattedInProperty(){
        return runsBattedIn.get();
    }
    
    public void setrunsBattedInProperty(int newVal){
        runsBattedIn.set(newVal);
    }

    public SimpleDoubleProperty stolenBasesProperty(){
        return stolenBases;
    }
    
    public double getStolenBasesProperty(){
        return stolenBases.get();
    }
    
    public void setStolenBasesProperty(int newVal){
        stolenBases.set(newVal);
    }

    private double getBattingAverage(){
        double val = (hits/atBats * 1000) / 1000;
        return (Double.isNaN(val)) ? 0 : new BigDecimal(val).setScale(3, RoundingMode.HALF_UP).doubleValue();
    }

    @Override
    public SimpleIntegerProperty getR_WColumn() {
        return runs;
    }

    @Override
    public SimpleIntegerProperty getHR_SVColumn() {
        return homeRuns;
    }

    @Override
    public SimpleIntegerProperty getRBI_KColumn() {
        return runsBattedIn;
    }

    @Override
    public SimpleDoubleProperty getSB_ERAColumn() {
        return stolenBases;
    }

    @Override
    public SimpleDoubleProperty getBA_WHIPColumn(){
        battingAverage.set(getBattingAverage());
        return battingAverage;
    }

    public void setAtBats(double cleanJsonValue) {
        atBats = cleanJsonValue;
    }

    public void setHomeRuns(int cleanJsonValue) {
        homeRuns.set(cleanJsonValue);
    }

    public void setRunsBattedIn(int cleanJsonValue) {
        
        runsBattedIn.set(cleanJsonValue);
    }

    public void setStolenBases(int cleanJsonValue) {
        stolenBases.set(cleanJsonValue);
    }
}
