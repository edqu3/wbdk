package wbdk.data;

/**
 *
 * @author edqu3
 */
public class GameProperties {
    
    public static final String[] MLB_TEAMS = {
        "ATL",
        "AZ",
        "CHC",
        "CIN",
        "COL",
        "LAD", 
        "MIA", 
        "MIL", 
        "NYM", 
        "PHI", 
        "PIT", 
        "SD", 
        "SF", 
        "STL", 
        "WAS"
    };
    
    public static enum MLB_TEAM{
        ATL,
        AZ,
        CHC,
        CIN,
        COL,
        LAD, 
        MIA, 
        MIL, 
        NYM, 
        PHI, 
        PIT, 
        SD, 
        SF, 
        STL, 
        WAS    
    }

    public static final String[] CONTRACT = {
        "X",
        "S1",
        "S2"
    };
    
}
