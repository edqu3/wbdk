package wbdk;

/**
 *
 * @author edqu3
 */
public class Properties {
    
    public final static String PLAYER_IMAGES_PATH = "/res/images/players/";
    public final static String FLAG_IMAGES_PATH   = "/res/images/flags/";
    public final static String IMAGE_NOT_FOUND    = "AAA_PhotoMissing.jpg";
    public final static String JPG_FILE_EXT       = ".jpg";
    public final static String PNG_FILE_EXT       = ".png";
    
}
