/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk;

import java.util.List;
import wbdk.data.Player;

/**
 * Type 
 * @author edqu3
 */
public interface ControllerType {
    
    void updateTable(List<Player> list);
    
}
