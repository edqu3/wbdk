package wbdk;

/**
 * parameters for a dialog box.
 * @author edqu3
 */
public class MessageDialogParamaters extends DialogParameters{

    private String message;
    private String buttonText;
    
    public MessageDialogParamaters setMessage(String message){
        this.message = message;
        return this;
    }
        
    public MessageDialogParamaters setButtonText(String buttonText){
        this.buttonText = buttonText;
        return this;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getMessage() {
        return message;
    }

}
